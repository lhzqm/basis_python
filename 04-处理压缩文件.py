from pathlib import Path
from zipfile import ZipFile

# with ZipFile("file.zip","w") as ZipFile:
#     for path in Path("test").rglob("*.*"):
#         ZipFile.write(path)

with ZipFile("file.zip") as zip:
    print(zip.namelist())
    info = zip.getinfo("test/__init__.py")
    print(info.file_size)
    print(info.compress_size)
    zip.extractall("extract")
