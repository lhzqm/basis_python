from pathlib import Path

# E:\01黑马\每日课程文件\work\Every_Day\0_work\basis_python

# C:\Python

# Path("C:\\Python\\test")
# Path(r"C:\Python\test")

# path = Path(r"test\__init__.py")
path = Path("test\\__init__.py")

print(path.exists())
print(path.is_file())
print(path.is_dir())

print(path.name)
print(path.stem)
print(path.suffix)
print(path.parent)

# path = path.with_name("file.txt")
path = path.with_suffix(".txt")
print(path.absolute())
print(path.exists())
