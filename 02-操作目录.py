from pathlib import Path

path = Path("test")
# path.exists()
# path.mkdir()
# path.rmdir()
# path.rename("test2")

# print(path.iterdir())

# for p in path.iterdir():
#     print(p)
paths = [p for p in path.iterdir() if p.is_dir()]
py_files = [p for p in path.rglob("*.py")]

print(py_files)